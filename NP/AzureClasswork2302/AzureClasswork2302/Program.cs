﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
namespace AzureClasswork2302
{
    class Program
    {
        static void Main(string[] args)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                "DefaultEndpointsProtocol=https;AccountName=koshsy;AccountKey=PMVhXfUn/fT+3+L6E9JxMhwIVBOrGq0ifH044bjGGM0rw4D5ITkBwoEgVFRlN5cTWcebYkJRzdyWycHX/QI9cg==;EndpointSuffix=core.windows.net");
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            CloudQueue queue = queueClient.GetQueueReference("myqueue");

            queue.CreateIfNotExists();

            CloudQueueMessage message = new CloudQueueMessage("Hello world");
            queue.AddMessage(message);
            Console.WriteLine("Press any key");
            Console.ReadKey();

            queue = queueClient.GetQueueReference("myqueue");
            CloudQueueMessage peekedMessage = queue.PeekMessage();
            Console.WriteLine(peekedMessage.AsString);
            Console.WriteLine(DateTime.Now);
            Console.WriteLine("Press any key");
            Console.ReadKey();

            queue = queueClient.GetQueueReference("myqueue");
            queue.FetchAttributes();
            int? cashedMessageCount = queue.ApproximateMessageCount;
            Console.WriteLine("Number of messages in queue "+cashedMessageCount);
            Console.WriteLine("Press any key");
            Console.ReadKey();

            foreach (CloudQueueMessage curMessage in
                queue.GetMessages(cashedMessageCount.Value, TimeSpan.FromMinutes(5)))
            {
                queue.DeleteMessage(curMessage);
            }
            Console.WriteLine("Press any key");
            Console.ReadKey();

            queue.Delete();
        }
    }
}
