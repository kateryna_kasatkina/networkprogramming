﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;

namespace WebApiTest2.Controllers
{
    public class SMSController : ApiController
    {
        public void  SendMail()
        {
            string to = "artem.konstantinovich@ukr.net";

            string subject = "test subject";
            string body = "test mail fro API";
            MailMessage msg = new MailMessage();

            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            msg.To.Add(to);
            msg.From = new MailAddress(appSettings["smtpuser"]);
            msg.Subject = subject;
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = body;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = false;
            msg.Priority = MailPriority.High;

            //Add the Creddentials
            SmtpClient client = new SmtpClient();
            client.Credentials = new NetworkCredential
                (appSettings["smtpuser"], appSettings["smtppassword"]);
            client.Port = int.Parse(appSettings["smtpport"]);
            client.Host = appSettings["smtpserver"];
            client.EnableSsl = true;
            client.Send(msg);
        }
    }
}
