﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FrontEnd._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Label ID="lbidinstance" runat="server" Text="Id instance"></asp:Label>
    <br />
    SETTING NAME
    <asp:TextBox ID="txtSetting" runat="server"></asp:TextBox>
    &nbsp
    <asp:Button ID="btnGet" runat="server" Text="Get" OnClick="btnGet_Click" />
    <asp:Label ID="lblSetingValue" runat="server" Text="Setting value = "></asp:Label>
     <br />
    ADD FILE :<asp:FileUpload ID="FileUpload1" runat="server" />
    &nbsp;<asp:Button ID="btnFileUpload" 
        runat="server" Text="Upload file" OnClick="btnFileUpload_Click">
          </asp:Button>
    <br />


    LIST FILES: 
    <br />
    <asp:Button ID="btnListFiles" runat="server" Text="Get List Files" OnClick="btnListFiles_Click" />
    <br />
    <asp:BulletedList ID="listfiles" runat="server"></asp:BulletedList>
    <br />
    DELETE FILE:
    <br />
    <asp:TextBox ID="txtFileNameToDel" runat="server">
    </asp:TextBox>
    <asp:Button ID="btnDeleteFile" runat="server" Text="Delete File" OnClick="btnDeleteFile_Click" />
</asp:Content>
