﻿using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FrontEnd
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbidinstance.Text = "Instance ID = " + RoleEnvironment.CurrentRoleInstance.Id;
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            string setting = txtSetting.Text.Trim();
            lblSetingValue.Text = RoleEnvironment.GetConfigurationSettingValue(setting);
        }

        protected void btnFileUpload_Click(object sender, EventArgs e)
        {
            LocalResource LR = RoleEnvironment.GetLocalResource("MyLocalStorage");
            FileStream FS = new FileStream(
                Path.Combine(LR.RootPath, FileUpload1.FileName),
                FileMode.OpenOrCreate);
            FS.Write(FileUpload1.FileBytes, 0, FileUpload1.FileBytes.Length);
            FS.Close();
            FS.Dispose();
        }

        protected void btnListFiles_Click(object sender, EventArgs e)
        {
            LocalResource LR = RoleEnvironment.GetLocalResource("MyLocalStorage");
            listfiles.Items.Clear();
            foreach (string file in Directory.GetFiles(LR.RootPath))
            {
                FileInfo FI = new FileInfo(file);
                listfiles.Items.Add(FI.Name);
            }          
        }

        protected void btnDeleteFile_Click(object sender, EventArgs e)
        {
            LocalResource LR = RoleEnvironment.GetLocalResource("MyLocalStorage");
            string filename = Path.Combine(
                LR.RootPath, txtFileNameToDel.Text.Trim()
                );
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }


    }
}