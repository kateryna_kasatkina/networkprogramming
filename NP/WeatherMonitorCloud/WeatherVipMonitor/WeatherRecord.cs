﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
namespace WeatherVipMonitor
{
    public enum WeatherInfoSource { WeatherService,Divice};
    public enum ReadingType {Temperature, Humidity, Pressure, Luminucity }
     public class WeatherRecord:TableEntity
    {
      
        public WeatherRecord(DateTime When,double Reading,WeatherInfoSource src,ReadingType type) {
            this.Reading = Reading;
            this.WeatherInfoSource=src;
            this.ReadingType=type;
            this.When = When;
            PartitionKey = string.Format("{0}{1}", When.Year, When.Month);
            RowKey = string.Format("{0}{1}{2}{3}{4}{5}{6}",
                src, type, When.Year, When.Day, When.Hour, When.Minute);

                }

        public double Reading { get; private set; }
        public object ReadingType { get; private set; }
        public object WeatherInfoSource { get; private set; }
        public DateTime When { get; private set; }
    }
}
