﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherVipMonitor
{
    
   public class WeatherDB
    {
        static CloudStorageAccount CloudAccount;
        static CloudTableClient CloudTable;
        public static CloudTable Table;

        public WeatherDB()
        {
            CloudAccount = CloudStorageAccount.Parse(Config.DBConnectionString);
            CloudTable = CloudAccount.CreateCloudTableClient();
            Table = CloudTable.GetTableReference(Config.TableName);
            Table.CreateIfNotExists();
        }

        public static DateTime CurrentDateTime
        {
            get {
                return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow,
              TimeZoneInfo.CreateCustomTimeZone("Kiev", TimeSpan.FromHours(2),
             "Kiev",
             "Kiev"));
            }
        }

        public static async void Record(ReadingType t, WeatherInfoSource src, double x)
        {
            var rd = new WeatherRecord(CurrentDateTime, x, src, t);
            await Table.ExecuteAsync(TableOperation.Insert(rd));
        }

        public static async Task<bool> RecordSuccess(ReadingType t, WeatherInfoSource src, double x)
        {
            var rd = new WeatherRecord(CurrentDateTime, x, src, t);
            await Table.ExecuteAsync(TableOperation.Insert(rd));
        }

        public static IEnumerable<WeatherRecord> GetData()
        {
            var q = new TableQuery<WeatherRecord>();
            return WeatherDB.Table.ExecuteQuery(q);
        }
        public static WeatherRecord[] GetCurrentReading(ReadingType type)
        {
            var data = (from z in WeatherDB.Table.CreateQuery<WeatherRecord>()
                        where z.When > DateTime.Now.AddMinutes(-10)
                        select z).ToString();
            var res = from z in data where z.ReadingType = type
                      group byte z.WeatherInfoSource into g
                      select g.OrderBeDecending(x => x.When).First();
        }
    }
}
