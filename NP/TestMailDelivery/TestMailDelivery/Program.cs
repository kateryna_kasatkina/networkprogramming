﻿using System.Net;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;
using System;

namespace TestMailDelivery
{
    class Program
    {
        public static class SmtpHelper
        {
           

            public static void SendEmail(string to, string subject, string body)
            {
                MailMessage msg = new MailMessage();

                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                msg.To.Add(to);
                msg.From = new MailAddress(appSettings["smtpuser"]);
                msg.Subject = subject;
                msg.SubjectEncoding = Encoding.UTF8;
                msg.Body = body;
                msg.BodyEncoding = Encoding.UTF8;
                msg.IsBodyHtml = false;
                msg.Priority = MailPriority.High;

                //Add the Creddentials
                SmtpClient client = new SmtpClient();
                client.Credentials = new NetworkCredential
                    (appSettings["smtpuser"], appSettings["smtppassword"]);
                client.Port = int.Parse(appSettings["smtpport"]);
                client.Host = appSettings["smtpserver"];
                client.EnableSsl = true;
                client.Send(msg);
            }
        }
        static void Main(string[] args)
        {
            try
            {
                SmtpHelper.SendEmail("artem.konstantinovich@ukr.net",
                    "test subject",
                    "test mail");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error occured :{0}",ex.Message);
            }
            Console.WriteLine("Press any key");
        }
    }
}
