﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parse
{
    class Program
    {
        public class ParseManager
        {
            public BGraph ParseFile(string path)
            {
                var graph = new BGraph();

                var lines = File.ReadAllLines(path);

                for (int i = 0; i < lines.Length; i++)
                {
                    // split line to segments between ','. First is the Bridge
                    string[] columns = lines[i].Split(',');
                    string[] nameID = columns[0].Split(']');
                    string bridgeName = nameID[0] + ']';
                    string bridgeId = nameID[1];
                    var newBridge = new Bridge(bridgeName, bridgeId);

                    for (int j = 1; j < columns.Length; j++)
                    {
                        string[] portParts = columns[j].Split(']');
                        string[] portCostLan = portParts[1].Split('=');
                        string[] portCost = portCostLan[1].Split(' ');
                        string number = portCost[1];
                        if (String.Equals(number[0], '0'))
                        {
                            number = number.Substring(1);
                        }
                        int pCost = Convert.ToInt32(number);
                        string LAN = portCostLan[2].Trim();
                        newBridge.BridgePorts.Add(new Port(j, pCost, LAN));
                    }
                    graph.vertcoll.Add(newBridge);
                }

                return graph;
            }
        }

        static void Main(string[] args)
        {
            var graph = new BGraph();

            var lines = File.ReadAllLines("D:/KateNP/networkprogramming/Parse/Parse/default.txt");

            for (int i = 0; i < lines.Length; i++)
            {
                Bridge currentBridge = new Bridge();
                Port currentPort = new Port();
                if(lines[i].Length>1)//string is not empty
                {
                    string[]substring=lines[i].Split(']');
                   // Console.WriteLine(substring.Length);
                    if(substring.Length==2)//if ']' in line
                    {
                        string [] names=substring[0].Split('.');
                        if (names.Length == 1)//if like [BRIDGE 2]
                        {
                            string bridgeName = names[0];
                            string[] forBridgeId = lines[i + 1].Split('=');
                            i++;
                            string bridgeId = forBridgeId[1].Trim();
                            currentBridge.BridgeName = bridgeName;
                            currentBridge.Bridge_Id = bridgeId;
                            graph.vertcoll.Add(currentBridge);
                        }

                        if(names.Length == 2)//if like [BRIDGE 2.Port 01]
                        {
                            string[] namesForPorts = names[1].Trim().Split(' ');
                            int portId = Convert.ToInt32(namesForPorts[1]);
                            currentPort.Port_Id = portId;
                            i++;

                            string[] forPortCost = lines[i].Split('=');
                            string portsId = forPortCost[1].Trim();
                            if (String.Equals(portsId[0], '0'))
                            {
                                portsId = portsId.Substring(1);
                            }
                            int pCost = Convert.ToInt32(portsId);
                            currentPort.Port_Id = pCost;
                            i++;

                            string[] forLan = lines[i].Split('=');
                            string lan = forLan[1].Trim();
                            currentPort.LAN = lan;
                            Bridge c=graph.vertcoll.Last();
                            c.AddPort(currentPort);
                        }
                    }
                    
                    //if (substring.Length == 1)
                    //{
                    //    if(lines[i].Trim().StartsWith("P"))
                    //    {
                    //        string[] forPortCost = lines[i].Split('=');
                    //        string portsId = forPortCost[1].Trim();
                    //        if (String.Equals(portsId[0], '0'))
                    //        {
                    //            portsId = portsId.Substring(1);
                    //        }
                    //        int pCost = Convert.ToInt32(portsId);
                    //        currentPort.Port_Id = pCost;
                    //    }

                    //    if(lines[i].Trim().StartsWith("C"))
                    //    {
                    //        string[] forLan = lines[i].Split('=');
                    //        string lan = forLan[1].Trim();
                    //        currentPort.LAN = lan;
                    //    }
                    //}
                    //if (currentBridge != null)
                    //{
                    //    graph.vertcoll.Add(currentBridge);
                    //}
                    //if(currentPort!=null)
                    //{
                    //    currentBridge.AddPort(currentPort);
                    //}
                }

               var result= graph;
            }
        }
    }
}
