﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    /// <summary>
    /// Implementation of a  vertex to be used in  graph
    /// </summary>
    public class Bridge
    {
        private string _bridge_Id;
        private List<Port> _bridgePorts;
        private string _bridgeName;
        public int RootPathCost { get; set; }

        private int _costToRoot;

        public int CostToRoot
        {
            get { return _costToRoot; }
            set { _costToRoot = value; }
        }


        public string BridgeName
        {
            get { return _bridgeName; }
            set { _bridgeName = value; }
        }

        public List<Port> BridgePorts
        {
            get { return _bridgePorts; }
            set { _bridgePorts = value; }
        }

        public string Bridge_Id
        {
            get { return _bridge_Id; }
            set { this._bridge_Id = value; }
        }

        public Bridge()
        {
            BridgePorts = new List<Port>();
        }

        public Bridge(string name, string id)
        {
            BridgeName = name;
            Bridge_Id = id;
            BridgePorts = new List<Port>();
        }

        public void AddPort(Port port)
        {
            BridgePorts.Add(port);
        }

        public void AddPorts(List<Port> ports)
        {
            BridgePorts.AddRange(ports);
        }

        public void RemovePort(Port port)
        {
            BridgePorts.Remove(port);
        }
    }
}
