﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Port
    {
        public int Port_Id { get; set; }
        public int PathCost { get; set; }
        public String LAN { get; set; }
        public PortState State { get; set; }

        public Port() { }
        public Port(int id, int pathCost, string lan)
        {
            Port_Id = id;
            PathCost = pathCost;
            LAN = lan;
        }
    }

    public enum PortState
    {
        None,
        RootPort,
        DesignatedPort,
        BlockedPort
    }
}
