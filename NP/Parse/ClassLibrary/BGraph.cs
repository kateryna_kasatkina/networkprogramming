﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class BGraph
    {
        public List<Bridge> vertcoll = null;
        Bridge v = null;

        public BGraph()
        {
            vertcoll = new List<Bridge>();
        }

        public BGraph(int size)
        {
            vertcoll = new List<Bridge>();
            for (int i = 0; i < size; i++)
            {
                v = new Bridge();
                v.Bridge_Id = i.ToString();
                vertcoll[i] = v;
            }
        }

        public BGraph(List<Bridge> coll)
        {
            vertcoll = coll;
        }
    }
}
