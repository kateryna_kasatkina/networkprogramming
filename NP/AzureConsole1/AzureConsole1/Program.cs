﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using Microsoft.WindowsAzure.Storage.Blob;

namespace AzureConsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            CloudStorageAccount account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=koshsy;AccountKey=sVGGCLzq/wniC3wvRrRoIbJ9WhNGNjI+5HVqU2TSwoHYKtIC6yVGRDzHbGIlBUkmx/Ah8SMV7raiNXIqUpUw/w==;EndpointSuffix=core.windows.net");
            CloudFileClient fileclient = account.CreateCloudFileClient();

            CloudFileShare share = fileclient.GetShareReference("logs");
            if (share.Exists())
            {
                CloudFileDirectory rootDir = share.GetRootDirectoryReference();
                CloudFileDirectory sampleDir = share.GetRootDirectoryReference();
                if (sampleDir.Exists())
                {
                    CloudFile file = sampleDir.GetFileReference("sample-file.txt");
                    if (file.Exists())
                    {
                        Console.WriteLine(file.DownloadTextAsync().Result);
                    }
                }
            }
            else
            {
                Console.WriteLine("Share is not exists");
                share.CreateIfNotExists();
                CloudFile sourseFile = share.GetRootDirectoryReference().GetFileReference("sample-file.txt");
                sourseFile.UploadText("File create by visual studio"+DateTime.Now.ToLongTimeString());

                CloudBlobClient blobClient = account.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("sample-container");
                container.CreateIfNotExists();
                CloudBlockBlob destBlob = container.GetBlockBlobReference("sample-blob.txt");

                string fileSas = sourseFile.GetSharedAccessSignature(new SharedAccessFilePolicy()
                {
                    Permissions = SharedAccessFilePermissions.Read,
                    SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24)
                });
                Uri fileSasUri = new Uri(sourseFile.StorageUri.PrimaryUri.ToString() + fileSas);
                destBlob.StartCopy(fileSasUri);
                Console.WriteLine("Source file contents:{0}",sourseFile.DownloadText());
                Console.WriteLine("Destination blob contents:{0}",destBlob.DownloadText());
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
