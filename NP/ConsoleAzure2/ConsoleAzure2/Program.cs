﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Auth;
namespace ConsoleAzure2
{
    class Program
    {
        static void Main(string[] args)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=koshsy;AccountKey=sVGGCLzq/wniC3wvRrRoIbJ9WhNGNjI+5HVqU2TSwoHYKtIC6yVGRDzHbGIlBUkmx/Ah8SMV7raiNXIqUpUw/w==;EndpointSuffix=core.windows.net");
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference("people");
            table.CreateIfNotExists();

            //CustomerEntity customer1 = new CustomerEntity("Harp", "Walter");
            //customer1.Email = "walter@gmail.com";
            //customer1.PhoneNumber = "425-555-2121";

            //TableOperation insertOperation = TableOperation.Insert(customer1);
            //table.Execute(insertOperation);




            //TableBatchOperation batchOperation = new TableBatchOperation();
            //CustomerEntity customer3 = new CustomerEntity("Harp", "Harry");
            //customer3.Email = "potter@gmail.com";
            //customer3.PhoneNumber = "324-234-2323";

            //CustomerEntity customer2 = new CustomerEntity("Harp", "Daiv");
            //customer2.Email = "vano@gmail.com";
            //customer2.PhoneNumber = "334-224-2223";

            //batchOperation.Insert(customer3);
            //batchOperation.Insert(customer2);

            //table.ExecuteBatch(batchOperation);

            //TableQuery<CustomerEntity> quere = new TableQuery<CustomerEntity>().
            //    Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "Harp"));

            //foreach (CustomerEntity entity in table.ExecuteQuery(quere))
            //{
            //    Console.WriteLine("{0}, {1}\t{2}\t{3}",
            //        entity.PartitionKey, entity.RowKey,
            //        entity.Email,entity.PhoneNumber);
            //}




            //TableQuery<CustomerEntity> rangeQuere = new TableQuery<CustomerEntity>().Where(
            //    TableQuery.CombineFilters(
            //        TableQuery.GenerateFilterCondition("PartitionKey",
            //        QueryComparisons.Equal,"Harp"),
            //        TableOperators.And,
            //        TableQuery.GenerateFilterCondition("RowKey",QueryComparisons.LessThan,"T")
            //    ));


            //foreach (CustomerEntity entity in table.ExecuteQuery(rangeQuere))
            //{
            //    Console.WriteLine("{0}, {1}\t{2}\t{3}",
            //        entity.PartitionKey, entity.RowKey,
            //        entity.Email, entity.PhoneNumber);
            //}

            TableOperation retriveOperation = TableOperation.Retrieve<CustomerEntity>("Harp", "Harry");
            TableResult retrieveResult = table.Execute(retriveOperation);
            if (retrieveResult.Result!=null)
                Console.WriteLine(((CustomerEntity)retrieveResult.Result).PhoneNumber);
            else
                Console.WriteLine("The phone number could not retrieved");

            CustomerEntity updateEntity = (CustomerEntity)retrieveResult.Result;

            if (updateEntity != null)
            {
                updateEntity.PhoneNumber = "+38-099-014-83-61";
                TableOperation insertorreplaceoperation = TableOperation.InsertOrReplace(updateEntity);
                table.Execute(insertorreplaceoperation);
                Console.WriteLine("Entity was update");
            }

            TableQuery<DynamicTableEntity> projectionQuere =
                new TableQuery<DynamicTableEntity>().Select(new string[] { "Email" });
            EntityResolver<string> resolver = (pk, rk, ts, props, etag) => props.ContainsKey("Email") ? props["Email"].StringValue : null;

            foreach (string projectedEmail in table.ExecuteQuery(projectionQuere,resolver,null,null))
            {
                Console.WriteLine(projectedEmail);
            }

            CustomerEntity deleteEntity = (CustomerEntity)retrieveResult.Result;

            if (deleteEntity != null)
            {
                TableOperation deleteOperations = TableOperation.Delete(deleteEntity);
                table.Execute(deleteOperations);
                Console.WriteLine("Entity Delete");
            }
            else
            {
                Console.WriteLine("Could not retreive the entity");
            }

            table.DeleteIfExists();

            Console.ReadKey();
        }

        public class CustomerEntity : TableEntity
        {
            public  CustomerEntity(string lastName, string firstName)
            {
                this.PartitionKey = lastName;
                this.RowKey = firstName;
            }

            public CustomerEntity() { }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }
        }

        public class KoshsyEntity : TableEntity
        {
            public KoshsyEntity(string passportCode, string index)
            {
                this.PartitionKey = index;
                this.RowKey = passportCode;
            }
            public KoshsyEntity() { }

            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Job { get; set; }


        }
    }
}
