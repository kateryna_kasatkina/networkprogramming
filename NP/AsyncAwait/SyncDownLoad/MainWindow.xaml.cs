﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SyncDownLoad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GetButtonClick(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text = "Starting sync download\n";

            var req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com/");
            req.Timeout = 2000;
            req.Method = "GET";

            try
            {
                var resp = (HttpWebResponse)req.GetResponse();
                dataTextBox.Text += "Sync completed\n";
                string headersText = resp.Headers.ToString();
                dataTextBox.Text += headersText;
            }
            catch (WebException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
