﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows;

namespace AsyncDownLoad_GUI_Problem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //private void GetButtonClick(object sender, RoutedEventArgs e)
        //{
        //    dataTextBox.Text += "Beginning download\n";
        //    var req = (HttpWebRequest)WebRequest.Create("http://uh433027-18.ukrdomen.com");
        //    req.Method = "GET";
        //    req.BeginGetResponse(
        //        asyncResult =>
        //        {
        //            var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
        //            string headersText = resp.Headers.ToString();
        //            //dataTextBox.Text += headersText; //exception - calling the interface component, that was created in the main thread;
        //                                             //current thread has no access to the components of main thread
        //            #region GUI Update WinForm Style
        //            //MainWindow myForm = this;
        //            //myForm.Invoke(new EventHandler(delegate
        //            //{
        //            //    dataTextBox.Text += headersText;
        //            //}));
        //            #endregion
        //        },
        //        null);
        //    dataTextBox.Text += "Download started async\n";
        //}

        //problem solving:
        #region GUI Update
        private void GetButtonClick2(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text += "Beginning download\n";
            var sync = SynchronizationContext.Current;
            var req = (HttpWebRequest)WebRequest.Create("http://uh433027-18.ukrdomen.com/api/Fruits");
            req.Method = "GET";
            req.BeginGetResponse(
                asyncResult =>
                {
                    var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
                   
                    string headersText = resp.Headers.ToString();
                 

                    sync.Post(
                        state => dataTextBox.Text += headersText,
                        null);

                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        throw new System.Exception(string.Format("Server error(HTTP{0}:{1}).", resp.StatusCode, resp.StatusDescription));
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        sync.Post(
                            state => dataTextBox.Text += Environment.NewLine + reader.ReadToEnd(),
                            null);
                        sync.Post(
                           state => dataTextBox.Text += Environment.NewLine + "Async doenload completed",
                           null);
                    }
                },
                null);

          
            dataTextBox.Text += "Download started async\n";
        }
        #endregion



        //private void GetButtonClick(object sender, RoutedEventArgs e)
        //{
        //    dataTextBox.Text += "Beginning download\n";
        //    var sync = SynchronizationContext.Current;
        //    //var req = (HttpWebRequest)WebRequest.Create("http://www.google.com");
        //    var req = (HttpWebRequest)WebRequest.Create("http://uh433027-14.ukrdomen.com/api/Fruits");
        //    req.Method = "GET";
        //    req.BeginGetResponse(
        //        asyncResult =>
        //        {
        //            var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
        //            string headersText = resp.Headers.ToString();
        //            if (resp.StatusCode != HttpStatusCode.OK)
        //                throw new Exception(String.Format("Server error (HTTP {0}: {1})", resp.StatusCode, resp.StatusDescription));
        //            StreamReader reader = new StreamReader(resp.GetResponseStream());
        //            sync.Post(
        //                state => dataTextBox.Text += Environment.NewLine + reader.ReadToEnd(),
        //                null);
        //            sync.Post(
        //               state => dataTextBox.Text += Environment.NewLine + "Async doenload completed",
        //               null);
        //            //exception - calling the interface component, that was created in the main thread;
        //            //current thread has no access to the components of main thread
        //            #region GUI Update WinForm Style
        //            //MainWindow myForm = this;
        //            //myForm.Invoke(new EventHandler(delegate
        //            //{
        //            //    dataTextBox.Text += headersText;
        //            //}));
        //            #endregion
        //        },
        //        null);
        //    dataTextBox.Text += "Download started async\n";
        //}
    }
}
