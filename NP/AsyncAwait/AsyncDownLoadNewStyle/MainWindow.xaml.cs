﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncDownLoadNewStyle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void GetButtonClick(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text += "Staring async download\n";
            await DoDownload();
            dataTextBox.Text += "Async download started\n";
        }

        async Task DoDownload()
        {

            var req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
            req.Method = "GET";
            var resp = (HttpWebResponse)await req.GetResponseAsync();

            dataTextBox.Text += resp.Headers.ToString();
            dataTextBox.Text += "Async download completed";
        }
    }


}
