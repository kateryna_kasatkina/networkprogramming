﻿using System.ServiceModel; //WCF сервисы 

namespace ServiceLibrary
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        string Method(string s);
    }

    public class MyService : IContract
    {
        public string Method(string s)
        {
            return s + " IIS";
        }
    }
}