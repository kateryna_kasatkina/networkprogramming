﻿using System;
using System.ServiceModel;


namespace ClientWCF
{
    class Program
    {
        static void Main(string[] args)
        {
            ChannelFactory<IContract> factory = new ChannelFactory<IContract>(new BasicHttpBinding(), new EndpointAddress("http://localhost:1323/MyService.svc"));

            IContract channel = factory.CreateChannel();

            string str = channel.Method("apple");
            Console.WriteLine(str);

            Console.WriteLine("Для завершения нажмите <Any Key>.");
            Console.ReadKey();
        }
    }
}
