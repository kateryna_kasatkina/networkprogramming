﻿using System.ServiceModel;

namespace ClientWCF
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        string Method(string s);
    }
}