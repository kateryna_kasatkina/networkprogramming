﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITask2.Models
{
    public class Letter
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

    }
}