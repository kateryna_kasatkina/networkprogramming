﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITask2.Models
{
    public class SMSmodel
    {
        public string PhoneNumber { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
        public string Password { get; set; }

    }
}