﻿using System.Web.Mvc;

namespace WebAPITask2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
