﻿using System.Web.Http;
using WebAPITask2.Models;

namespace WebAPITask2.Controllers
{
    public class SMSController : ApiController
    {
        [HttpPost]
        public void SendSMS([FromBody]SMSmodel forSms)
        {
            SendHelper.SendHelper.SendSMS(forSms);
        }
    }
}
