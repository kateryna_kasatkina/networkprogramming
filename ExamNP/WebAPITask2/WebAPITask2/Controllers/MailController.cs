﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Web.Http;
using WebAPITask2.Models;
using WebAPITask2.SendHelper;

namespace WebAPITask2.Controllers
{
    public class MailController : ApiController
    {
        [HttpPost]
        public void SendMail([FromBody]Letter letter)
        {
            SendHelper.SendHelper.SendEmail(letter.To, letter.Subject, letter.Body);
        }
    }
}
