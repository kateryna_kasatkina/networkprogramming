﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace SignalRChat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Chat()
        {
            ViewData["IP"] = Request.UserHostAddress;
            return View();
        }
    }
}